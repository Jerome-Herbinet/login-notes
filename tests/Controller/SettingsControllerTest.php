<?php

declare(strict_types=1);

namespace OCA\LoginNotes\Tests\Controller;

use OCA\LoginNotes\AppInfo\Application;
use OCA\LoginNotes\Controller\SettingController;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataResponse;
use OCP\IConfig;
use OCP\IRequest;
use PHPUnit\Framework\MockObject\MockObject;
use ChristophWurst\Nextcloud\Testing\TestCase;

class SettingsControllerTest extends TestCase
{
	private SettingController $controller;
	/**
	 * @var IConfig|MockObject
	 */
	private $config;

	public function setUp(): void
	{
		parent::setUp();

		$request = $this->createMock(IRequest::class);
		$this->config = $this->createMock(IConfig::class);
		$this->controller = new SettingController(Application::APP_NAME, $request, $this->config);
	}

	public function setAppValueDataProvider(): array
	{
		return [
			[null, null, []],
			[
				true,
				true,
				[
					['login_notes', 'centered', 'yes'],
					['login_notes', 'github_markdown', 'yes']
				]
			],
			[null, true, [
				['login_notes', 'github_markdown', 'yes']
			]],
			[false, null, [
				['login_notes', 'centered', 'no'],
			]],
			[false, true, [
				['login_notes', 'centered', 'no'],
				['login_notes', 'github_markdown', 'yes']
			]],
			[true, false, [
				['login_notes', 'centered', 'yes'],
				['login_notes', 'github_markdown', 'no']
			]],
			[false, false, [
				['login_notes', 'centered', 'no'],
				['login_notes', 'github_markdown', 'no']
			]]
		];
	}

	/**
	 * @dataProvider setAppValueDataProvider
	 * @param bool $centered
	 */
	public function testSetAppValue(?bool $centered, ?bool $github_markdown, array $params): void
	{
		$this->config->expects(self::exactly(count($params)))->method('setAppValue')->withConsecutive(...$params);
		$response = new DataResponse([], Http::STATUS_OK);
		self::assertEquals($response, $this->controller->set($centered, $github_markdown));
	}
}

<?php

declare(strict_types=1);

namespace OCA\LoginNotes\Tests\Settings;

use OCA\LoginNotes\AppInfo\Application;
use OCA\LoginNotes\Manager;
use OCA\LoginNotes\Model\Note;
use OCA\LoginNotes\Settings\AdminSettings;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\DB\Exception;
use OCP\IConfig;
use ChristophWurst\Nextcloud\Testing\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class AdminSettingsTest extends TestCase
{
	/**
	 * @var Manager|MockObject
	 */
	private $manager;
	/**
	 * @var IInitialState|MockObject
	 */
	private $initialStateService;
	/**
	 * @var IConfig|MockObject
	 */
	private $config;
	private AdminSettings $adminSettings;

	public function setUp(): void
	{
		parent::setUp();
		$this->manager = $this->createMock(Manager::class);
		$this->initialStateService = $this->createMock(IInitialState::class);
		$this->config = $this->createMock(IConfig::class);
		$this->adminSettings = new AdminSettings($this->manager, $this->initialStateService, $this->config);
	}

	public function testGetPriority() : void
	{
		self::assertEquals(10, $this->adminSettings->getPriority());
	}

	public function testGetSection() : void
	{
		self::assertEquals('additional', $this->adminSettings->getSection());
	}

	public function dataForTestGetForm(): array
	{
		return [
			[true, false, [new Note()]],
			[true, true, [new Note()]],
			[false, false, [new Note()]],
			[false, true, [new Note()]],
		];
	}

	/**
	 * @dataProvider dataForTestGetForm
	 * @param bool $centeredBool
	 * @param bool $github_markdown
	 * @param array $notes
	 * @throws Exception
	 */
	public function testGetForm(bool $centeredBool, bool $github_markdown, array $notes): void
	{
		$this->config->expects($this->exactly(2))->method('getAppValue')
			->withConsecutive(
				[Application::APP_NAME, 'centered', 'no'],
				[Application::APP_NAME, 'github_markdown', 'no']
			)
			->willReturnOnConsecutiveCalls($centeredBool ? 'yes' : 'no', $github_markdown ? 'yes' : 'no');
		$this->initialStateService->expects(self::exactly(4))->method('provideInitialState')->withConsecutive(
			['centered', $centeredBool],
			['github_markdown', $github_markdown],
			['notes', $notes],
			['pages', ['totp' => class_exists(\OCA\TwoFactorTOTP\AppInfo\Application::class), 'saml' => false, 'u2f' => false, 'email' => false, 'twofactor_nextcloud_notification' => false]]
		);
		$this->manager->expects(self::once())->method('getNotes')->with()->willReturn($notes);
		$response = new TemplateResponse('login_notes', 'admin');
		self::assertEquals($response, $this->adminSettings->getForm());
	}
}

<?php

declare(strict_types=1);

namespace OCA\LoginNotes\Tests\Listeners;

use OCA\LoginNotes\AppInfo\Application;
use OCA\LoginNotes\Listeners\InjectNote;
use OCA\LoginNotes\Manager;
use OCP\AppFramework\Http\Events\BeforeTemplateRenderedEvent;
use OCP\AppFramework\Services\IInitialState;
use OCP\EventDispatcher\Event;
use OCP\IConfig;
use OCP\IRequest;
use PHPUnit\Framework\MockObject\MockObject;
use ChristophWurst\Nextcloud\Testing\TestCase;

class InjectNoteTest extends TestCase
{
	/**
	 * @var IConfig|MockObject
	 */
	private $config;
	/**
	 * @var IRequest|MockObject
	 */
	private $request;
	private InjectNote $listener;

	/** @var IInitialState|MockObject */
	private $initialState;

	/** @var Manager|MockObject */
	private $manager;

	public const UID = 'someUID';
	/**
	 * @var BeforeTemplateRenderedEvent|MockObject
	 */
	private $event;

	public function setUp(): void
	{
		parent::setUp();
		$this->request = $this->createMock(IRequest::class);
		$this->config = $this->createMock(IConfig::class);
		$this->event = $this->createMock(BeforeTemplateRenderedEvent::class);
		$this->initialState = $this->createMock(IInitialState::class);
		$this->manager = $this->createMock(Manager::class);

		$this->listener = new InjectNote($this->request, $this->config, $this->initialState, $this->manager);
	}

	public function testHandleUnrelated(): void
	{
		$event = new Event();
		$this->request->expects($this->never())->method('getRequestUri');
		$this->listener->handle($event);
	}

	public function testHandleOtherPath(): void
	{
		$this->request->expects($this->once())->method('getRequestUri')->with()->willReturn('/someotherpath');
		$this->manager->expects($this->never())->method('getNotes')->with();
		$this->listener->handle($this->event);
	}

	public function testHandleLoginPath(): void
	{
		$this->request->expects($this->once())->method('getRequestUri')->with()->willReturn('/login');
		$this->manager->expects($this->once())->method('getNotes')->with()->willReturn([]);
		$this->config->expects($this->exactly(2))->method('getAppValue')
			->withConsecutive([Application::APP_NAME, 'centered', 'no'], [Application::APP_NAME, 'github_markdown', 'no'])
			->willReturnOnConsecutiveCalls('no', 'no');
		$this->initialState->expects($this->exactly(3))->method('provideInitialState')->withConsecutive(['centered', false], ['github_markdown', false], ['notes', []]);
		$this->listener->handle($this->event);
	}
}
